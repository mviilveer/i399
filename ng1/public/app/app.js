(function () {
    'use strict';

    var app = angular.module('app', ['ngRoute']);

    app.constant('RESOURCES', (function () {
        var resource = 'http://localhost:3000';
        return {
            CONTACTS: resource + '/api/contacts/',
            DELETE_CONTACTS: resource + '/api/contacts/delete'
        }
    })());
})();