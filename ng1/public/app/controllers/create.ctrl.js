(function () {
    'use strict';

    angular.module('app').controller('CreateCtrl', Ctrl);

    function Ctrl($http, $location, RESOURCES) {
        var vm = this;
        vm.save = save;

        vm.errors = [];
        vm.form = {
            name: null,
            phone: null
        };

        function save() {
            $http.post(RESOURCES.CONTACTS, vm.form).then(function () {
                $location.path('/list');
            }).catch(function (result) {
                vm.errors = result.data.errors;
            });
        }
    }
})();
