(function () {
    'use strict';

    angular.module('app').controller('SearchCtrl', Ctrl);

    function Ctrl($http, modalService, RESOURCES, contacts) {
        var vm = this;
        vm.contacts = contacts.data;
        vm.searchString = null;
        vm.selected = [];
        vm.remove = remove;
        vm.search = search;
        vm.removeSelected = removeSelected;

        function search() {
            var searchServer = RESOURCES.CONTACTS;
            $http.get(searchServer, {params: { q: vm.searchString }}).then(function (result) {
                vm.contacts = result.data;
            });
        }

        function remove(id) {
            modalService.confirm().then(function () {
                $http.delete(RESOURCES.CONTACTS + id)
            }).then(search);
        }


        function removeSelected() {
            if (Object.keys(vm.selected).length > 0) {
                modalService.confirm().then(function () {
                    $http.post(RESOURCES.DELETE_CONTACTS, Object.keys(vm.selected))
                }).then(search);
            }
        }
    }
})();
