(function () {
    'use strict';

    angular.module('app').controller('UpdateCtrl', Ctrl);

    function Ctrl($http, $location, RESOURCES, contact) {
        var vm = this;
        vm.save = save;

        vm.errors = [];

        vm.contact = contact.data;
        vm.form = {
            name: vm.contact.name,
            phone: vm.contact.phone
        };

        function save() {
            $http.put(RESOURCES.CONTACTS + vm.contact._id, vm.form).then(function () {
                $location.path('/list');
            }).catch(function (result) {
                vm.errors = result.data.errors;
            });
        }
    }
})();
