(function () {
    'use strict';

    angular.module('app').config(routeConfig);

    function routeConfig($routeProvider) {
        $routeProvider.when('/search', {
            templateUrl : 'app/templates/list.html',
            controller: 'SearchCtrl',
            controllerAs: 'vm',
            resolve: {
                contacts: function ($http, RESOURCES) {
                    return $http.get(RESOURCES.CONTACTS);
                }
            }
        }).when('/new', {
            templateUrl : 'app/templates/new.html',
            controller: 'CreateCtrl',
            controllerAs: 'vm'
        }).when('/view/:id', {
            templateUrl : 'app/templates/update.html',
            controller: 'UpdateCtrl',
            controllerAs: 'vm',
            resolve: {
                contact: function ($http, $route, RESOURCES) {
                    return $http.get(RESOURCES.CONTACTS + $route.current.params.id)
                }
            }
        }).otherwise('/search');
    }

})();

