'use strict';

const express = require('express');
const app = express();
const Dao = require('./dao.js');
const bodyParser = require('body-parser');

app.use(express.static('public'));
app.use(bodyParser.json());
app.use((request, response, next) => {
    response.set('Content-Type', 'application/json');
    next();
});

const url = 'mongodb://mihkel:mihkel@ds161890.mlab.com:61890/i399';
const dao = new Dao();

dao.connect(url).then(() => app.listen(3000));

const contactApi = {
    insert: (request, response) => {
        dao.insert(request.body).then(data => {
            response.json(data);
        }).catch(error => {
            console.log(error);
            response.end('error' + error);
        });
    },
    update: (request, response) => {
        const id = request.params.id;
        dao.update(id, request.body).then(data => {
            response.json(data);
        }).catch(error => {
            console.log(error);
            response.end('error' + error);
        });
    },
    remove: (request, response) => {
        const id = request.params.id;
        dao.remove(id).then(data => {
            response.json(data);
        }).catch(error => {
            console.log(error);
            response.end('error' + error);
        });
    },
    removeMany: (request, response) => {
        dao.removeAll(request.body).then(data => {
            response.json(data);
        }).catch(error => {
            console.log(error);
            response.end('error' + error);
        });
    },
    find: (request, response) => {
        dao.findAll(request.query.q).then(data => {
            response.json(data);
        }).catch(error => {
            console.log(error);
            response.end('error' + error);
        });
    },
    findOne: (request, response) => {
        const id = request.params.id;
        dao.findOne(id).then(data => {
            response.json(data);
        }).catch(error => {
            console.log(error);
            response.end('error' + error);
        });
    }
};

app.get('/api/contacts', contactApi.find);
app.get('/api/contacts/:id', contactApi.findOne);
app.delete('/api/contacts/:id', contactApi.remove);
app.post('/api/contacts', contactApi.insert);
app.post('/api/contacts/delete', contactApi.removeMany);
app.put('/api/contacts/:id', contactApi.update);
