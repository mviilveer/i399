import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppComponent }  from './app.cmp';
import { ContactService } from "./contact.srv";
import { RouterModule } from "@angular/router";
import { routes } from "./routes";
import { ListComponent } from "./list/list.cmp";
import {MenuComponent} from "./menu/menu.cmp";
import {NewComponent} from "./new/new.cmp";
import {UpdateComponent} from "./update/update.cmp";

@NgModule({
    imports: [ BrowserModule, HttpModule, FormsModule, RouterModule.forRoot(routes, { useHash: true }) ],
    declarations: [ AppComponent, ListComponent, MenuComponent, NewComponent, UpdateComponent ],
    providers: [ ContactService ],
    bootstrap: [ AppComponent ]
})
export class AppModule { }