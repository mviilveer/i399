import { Routes } from '@angular/router';

import { ListComponent } from './list/list.cmp';
import {NewComponent} from "./new/new.cmp";
import {UpdateComponent} from "./update/update.cmp";

export const routes: Routes = [
    { path: 'search', component: ListComponent },
    { path: 'list', component: ListComponent },
    { path: 'new', component: NewComponent },
    { path: 'update/:id', component: UpdateComponent },
    { path: '', redirectTo: 'search', pathMatch: 'full' }
];